#-------------------------------------------------------------------------------
# Name:        Parser Note Excel
#
# Author:      rpx
#
# Created:     12/04/2015
# Copyright:   (c) Razvan Popa 2015
#-------------------------------------------------------------------------------

from openpyxl import load_workbook

class NotaMaterie:
    def __init__(self, nota, nume_materie):
        self.nota = nota
        self.nume_materie = nume_materie

class Elev:
    def __init__(self, nr_matricol, nume, prenume):
        self.nr_matricol = nr_matricol
        self.nume = nume
        self.prenume = prenume

class ParserNoteExcel:
    def __init__(self):
        self.elevi = {}

    def adaugaNote(self, nume_materie, cale_fisier):
        wb = load_workbook(cale_fisier)
        columns = wb.active.columns
        for i in range(1, len(columns[0])):
            elev = Elev(columns[0][i].value, columns[1][i].value, columns[2][i].value)
            nm = NotaMaterie(columns[3][i].value, nume_materie)
            if self.elevi.get(elev):
                self.elevi[elev].append(nm)
            else:
                self.elevi[elev] = [nm]

def main():
    pne = ParserNoteExcel()
    pne.adaugaNote('Super_Materie', r'C:\Users\iungureanu\Documents\iungureanu.bitbucket.org\static\Scripts\note_materie.xlsx')
    for elev in pne.elevi:
        print elev.nr_matricol, elev.nume, elev.prenume + ':'
        for materie in pne.elevi[elev]:
            print '\t', materie.nume_materie, materie.nota

if __name__ == '__main__':
    main()