#-------------------------------------------------------------------------------
# Name:        Parse Facebook Posts From Group
#
# Author:      Razvan Popa
#
# Created:     30/05/2015
#-------------------------------------------------------------------------------

from time import strptime, strftime
import urllib2
import json

import sentimentAnalyser as SA

__access_token = 'CAACEdEose0cBAJf8qMd2LuOrkP0zetZCOrzZBwhHwTRqZC93bDv9GuXPb2hGlC3YJQmpq5SfWUyMOgiR07aVyX68BZBtNJ93JaaduWmezXWNxBZBTcfZAMfMWpQGt9fzdghnDwyDVUwBAZC8znJo6o2Agwf8Dfvqz01ZBDZB7MqMB1a69XXbzt8l9QT9mMsdohYcWNbYZAFSoniB3kAa3rwZAZBL'

__user_id = '1628440134034928'

class FacebookComment:
    def __init__(self, user_id, message):
        self.user_id = user_id
        self.message = message

class FacebookPost:
    def __init__(self, post_id, user_id, updated_time, message, tag, comments):
        self.post_id = post_id
        self.user_id = user_id
        self.updated_time = updated_time
        self.message = message
        self.tag = tag
        self.comments = comments

    def __str__(self):
        conv = 'post_id: ' + self.post_id + '\nuser_id: ' + self.user_id + '\n'
        conv += 'updated_time: ' + self.updated_time + '\n'
        conv += 'tag: ' + self.tag + '\n'
        conv += 'message: ' + self.message + '\n'
        conv += 'Comments:\n'
        for comment in self.comments:
            conv += '\tuser_id: ' + comment.user_id + '\n'
            conv += '\tmessage: ' + comment.message + '\n\n'
        return conv

__tags_dict = {'JAVA' : 'PA', 'DIDACTICA' : 'PEDAGOGIE', 'PSIH-ED' : 'PEDAGOGIE', 'DIDACTICA INFORMATICII' : 'PEDAGOGIE', 'PSIHOLOGIE' : 'PEDAGOGIE', 'PROBABILITATI&STATISTICA' : 'PS', 'RETELE' : 'RC', 'WEB' : 'TW', 'ASM' : 'ACSO', 'MATE' : 'MATEMATICA', 'ALGORITMICA GRAFURILOR' : 'AG', 'CRIPTOGRAFIE' : 'CC', 'ALGORITMI GENETICI' : 'AG'}

__tags = set(['WEB', 'IP', 'PF', 'FAI', 'DIDACTICA', 'SD', 'MATE', 'JAVA', 'PSIH-ED', 'LFAC', 'ACSO', 'PA ', 'PSIHOLOGIE', 'ENG', 'PROBABILITATI&STATISTICA', 'BD', 'PLP', 'RETELE', 'LOGICA', 'GRAFURI', 'PL', 'WEB', 'ASM', ' POO ', 'SO', 'MATLAB', 'CC', 'RC', 'SGBD', 'PEDAGOGIE', 'MATE', 'POO', 'ALGORITMICA GRAFURILOR', 'PA', 'TW', 'PS', 'CRIPTOGRAFIE', 'ALGORITMI GENETICI', 'AG'])

def getTag(message):
    ib = message.find('[')
    ie = message.find(']')
    if ib == -1 or ie == -1:
        return ''
    ret_tag = message[ib : ie + 1]
    for tag in __tags:
        if tag in ret_tag:
            return  __tags_dict[tag] if __tags_dict.get(tag) else tag
    return ''

def getUpdates(last_update, group_id, acces_token):
    updates = []
    request = 'https://graph.facebook.com/' + group_id + '/feed?access_token=' + acces_token
    while request != '':
        try:
            response = urllib2.urlopen(request)
            data = json.loads(response.read())
            for entry in data['data']:
                if strptime(entry['updated_time'], '%Y-%m-%dT%H:%M:%S+0000') < strptime(last_update, '%Y-%m-%dT%H:%M:%S+0000'):
                    break
                comments = []
                if entry.get('comments'):
                    for comment in entry['comments']['data']:
                        comments.append(FacebookComment(comment['from']['id'], comment['message']))
                if not entry.get('message'):
                    continue
                message = entry['message'].strip()
                tag = '[' + getTag(message) + ']'
                if tag != '[]':
                    updates.append(FacebookPost(entry['id'], entry['from']['id'], entry['updated_time'], message, tag, comments))
            request = data['paging']['next'] if (data.get('paging') and data['paging'].get('next')) else ''
        except Exception as exc:
            print 'Graph api request has failed! => ' + repr(exc)
    return updates

def findRelevantGroups(user_id, access_token):
    groups_id = []
    try:
        response = urllib2.urlopen('https://graph.facebook.com/' + user_id + '/groups?access_token=' + access_token)
        data = json.loads(response.read())
        for group in data['data']:
            groups_id.append((group['name'], group['id']))
    except Exception as exc:
        print 'Graph api request has failed! => ' + repr(exc)

    return groups_id

def main():
    updates = getUpdates('2010-03-29T11:20:40+0000', '392108347584096', __access_token)
    materii = {}
    for fp in updates:
        if fp.tag != '':
            if materii.get(fp.tag):
                materii[fp.tag].append(fp.message)
            else:
                materii[fp.tag] = [fp.message]
            for fc in fp.comments:
                materii[fp.tag].append(fc.message)

    print materii.keys()
    for materie in materii:
        print materie + ':\t' + str(SA.getMean(materii[materie]))

if __name__ == '__main__':
    main()
