#-------------------------------------------------------------------------------
# Name:        Schedule recommender
#
# Author:      Razvan
#
# Created:     01/06/2015
# Copyright:   (c) Razvan 2015
#-------------------------------------------------------------------------------

__proportii = {'Analiza retelelor media sociale' : ( 0.6, 0.4),
    'Arhitectura calculatoarelor si sistemelor de operare' : (0.4, 0.6),
    'Algoritmica grafurilor' : (1, 0),
    'Animatie 3D: algoritmi si tehnici fundamentale' : (0.3, 0.7),
    'Aspecte computationale in teoria numerelor' : ( 0.9, 0.1),
    'Baze de date' : (0.6, 0.4),
    'Calcul numeric' : (0.95, 0.05),
    'Calculabilitate, decidabilitate si complexitate' : (0.0, 0.1),
    'Capitole speciale de sisteme de operare' : (0.1, 0.9),
    'Coduri si criptografie' : (0.5, 0.5),
    'Cloud computing' : (0.05, 0.95 ),
    'Dezvoltare de Aplicatii pe Platforma .NET' : (0.9, 0.1),
    'Dezvoltarea de aplicatii vizuale multiplatforma' : (0.95, 0.05),
    'Dezvoltarea de aplicatii web la nivel de client': (0.05, 0.95),
    'Didactica informaticii' : (0.8, 0.2),
    'Fundamente algebrice ale informaticii' : (0.9, 0.1),
    'Grafica pe calculator' : (0.2, 0.8),
    'Ingineria programarii' : (0.4, 0.6),
    'Inteligenta artificiala' : (0.7, 0.3),
    'Introducere in programare' : (0.35, 0.65),
    'Limba engleza I' : (0, 1),
    'Limba engleza II' : (0, 1),
    'Limba engleza III' : (0, 1),
    'Limba engleza IV' : (0, 1),
    'Limbaje formale, automate si compilatoare' : (0.9, 0.1),
    'Logica pentru informatica' : (0.98, 0.02),
    'Managementul clasei de elevi' : (0.98, 0.02),
    'Matematica' : (1, 0),
    'Matematica pentru concursuri' : (1, 0),
    'Modele continue si Matlab' : (0.8, 0.2),
    'Pedagogie' : (1, 0),
    'Practica SGBD' : (0.1, 0.9),
    'Probabilitati si statistica' : (0.7, 0.3),
    'Programare MS-Office' : (0, 1),
    'Programare avansata' : (0.15, 0.85),
    'Programare bazata pe reguli' : (0.85, 0.15),
    'Programare competitiva I' : (0.5, 0.5),
    'Programare competitiva II' : (0.5, 0.5),
    'Programare functionala' : (0.3, 0.7),
    'Programare logica' : (0.4, 0.6),
    'Programare multimedia' : (0.1, 0.9),
    'Programare orientata-obiect' : (0.6, 0.4),
    'Proiectarea algoritmilor' : (0.95, 0.05),
    'Retele de calculatoare' : (0.1, 0.9),
    'Retele Petri si aplicatii' : (0.6, 0.4),
    'Securitatea informatiei' : (0.3, 0.7),
    'Sisteme de operare' : (0.3, 0.7),
    'Structuri de date' : (0.8, 0.2),
    'Tehnici de programare pe platforma Android' : (0.1, 0.9),
    'Tehnologii Web' : (0.25, 0.75),
    'Topici speciale de programare .NET' : (0.15, 0.85)}

def getScore(materii_note, materii):
    score_teorie = 0.0
    score_practica = 0.0
    count = 0
    for (materie, nota) in materii_note:
        if __proportii.get(materie):
            score_teorie += __proportii[materie][0]
            score_practica += __proportii[materie][1]
            count += 1

    if count == 0:
        return materii

    sm = []
    score_teorie = score_teorie / count
    score_practica = score_practica / count
    for materie in materii:
        if __proportii.get(materie):
            st = __proportii[materie][0]
            sp = __proportii[materie][1]
            sm.append(((score_teorie - st) * (score_teorie - st) + (score_practica - sp) * (score_practica - sp), materie))

    return [materie for (scor, materie) in sorted(sm)]

def main():
    print getScore([('Fundamente algebrice ale informaticii', 4), ('Sisteme de operare', 7), ('Proiectarea algoritmilor' , 10), ('Programare orientata-obiect', 10), ('Programare functionala', 10), ('Probabilitati si statistica', 9)], ['Tehnici de programare pe platforma Android', 'Programare bazata pe reguli', 'Topici speciale de programare .NET'])

if __name__ == '__main__':
    main()
