from acre.models import Note, Student
from django.db import connections
from django.contrib.auth.models import User
from acre.models import Materie, Catalog


def get_note(request):
    u = User.objects.filter(username=request.session['username'])
    st = Student.objects.filter(user=u[0])
    if len(st) == 0:
        return []
    nr_matricol = st[0].nr_matricol

    db_wrap = connections['default']
    catalog = db_wrap.get_collection('acre_catalog')
    note = catalog.find({'note.nr_matricol': nr_matricol})

    result = []
    for n in note:
        for i in n['note']:
            if i['nr_matricol'] == nr_matricol:
                result.append({'disciplina': n['nume_materie'], 'nota': i['nota'], 'an': n['an_studiu'],
                               'semestru': n['semestru']})

    return result

def get_obiect_chart(request):
    u = User.objects.filter(username=request.session['username'])
    st = Student.objects.filter(user=u[0])
    if len(st) == 0:
        return []
    an_studiu=st[0].an_studiu

    db_wrap = connections['default']
    catalog = db_wrap.get_collection('acre_catalog')
    note = catalog.find({'an_studiu': an_studiu})
    ob=[]
    for nota in note:
        serie={}
        mat=nota['nume_materie']
        serie['nume']=mat
        note_mat=[]
        frecvente=[]
        for elem in nota['note']:
            note_mat.append(elem['nota'])
        for i in range(1,10):
            frecvente.append(note_mat.count(i))
        serie['data']=frecvente
        ob.append(serie)

    return ob