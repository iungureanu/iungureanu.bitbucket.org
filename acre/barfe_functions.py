from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db import connections
from acre.models import posturi

def get_posts(request, nume_tag):
	if nume_tag is None:
		post = posturi.objects.filter()
	else:
		post = posturi.objects.filter(tag=nume_tag)
	result = []
	idx = 0
	for p in post:
		result.append({'id':idx,'post':p})
		idx = idx + 1
	return (result)

