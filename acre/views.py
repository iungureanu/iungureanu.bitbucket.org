from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from acre.models import Student, Fbg, fb_recomand, posturi
from django.http import HttpResponse
import form_functions
import account_functions as af
import orar_functions as of
import profesor_functions as pf
import note_functions as nf
import barfe_functions as bf
from django.db import connections
import time
from time import strptime, strftime
import recomandare_prof as rp
import recomandare_note as rn
from settings import FACEBOOK_APP_ID, FACEBOOK_SECRET_KEY
import json

def login_page_view(request):
    return render(request, 'login_page.html')


def index(request):

    logged = False
    is_prof = False
    if 'username' in request.session:
        logged = True
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False
    username = 'NoOne'
    orar_link = '<a href="/orar">'
    orar = False
    p = []
    if logged:
        if 'grupa' in request.session:
            print request.session['grupa']
            orar_link = '<a href="orar/' +  request.session['grupa'] + '">'
            orar = True
        else:
            print 'nu grupa'

        post = posturi.objects.filter()
        
        lg = len(post)
        for i in range(5):
            print lg - i -1 
            p.append({'tag':post[(lg-i-1)].tag, 'mesaj':post[(lg - i - 1)].mesaj.mesaj})
        #print p

    return render(request, 'index.html', {'logged': logged, 'username': username,'orar':orar, 'orar_link': orar_link, 'is_prof':is_prof,'postari':p})


def register_user(request):
    print 'user'
    if 'email' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the email', 'username': None,'logged': False})
    if 'passwordbranza' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the password', 'username': None,'logged': False})
    if 'grupa' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the group', 'username': None,'logged': False})
    if 'an' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the year', 'username': None,'logged': False})
    if 'matricol' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete matricol number', 'username': None,'logged': False})  



    if request.POST['email'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the email', 'username': None,'logged': False})
    if request.POST['passwordbranza'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the password', 'username': None,'logged': False})
    if request.POST['grupa'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the group', 'username': None,'logged': False})
    if request.POST['an'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the year', 'username': None,'logged': False})
    if request.POST['matricol'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete matricol number', 'username': None,'logged': False})  
    request = af.register(request,'elev')

    if request.session['result'] == 0:
        return render(request, 'registered.html',
                      {'error': False, 'error_message': None, 'username': request.session['first_name'],
                       'logged': True})
    if request.session['result'] == -1:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'User already exists', 'username': None, 'logged': False})
    if request.session['result'] == -2:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'Activate your account from email', 'username': None, 'logged': False})
    if request.session['result'] == -3:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'Username or passowrd incorect', 'username': None,
                       'logged': False})

def register_prof(request):
    print 'user'
    if 'email' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the email', 'username': None,'logged': False})
    if 'passwordbranza' not in request.POST:
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the password', 'username': None,'logged': False})


    if request.POST['email'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the email', 'username': None,'logged': False})
    if request.POST['passwordbranza'] == '':
        return render(request, 'registered.html',{'error': True, 'error_message': 'You must complete the password', 'username': None,'logged': False})
   
    request = af.register(request,'prof')

    if request.session['result'] == 0:
        return render(request, 'registered.html',
                      {'error': False, 'error_message': None, 'username': request.session['first_name'],
                       'logged': True})
    if request.session['result'] == -1:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'User already exists', 'username': None, 'logged': False})
    if request.session['result'] == -2:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'Inactive user', 'username': None, 'logged': False})
    if request.session['result'] == -3:
        return render(request, 'registered.html',
                      {'error': True, 'error_message': 'Username or passowrd incorect', 'username': None,
                       'logged': False})



def user_login(request):
    request = af.user_login2(request)
    return redirect('index')


def user_logout(request):
    request = af.log_out(request)
    return redirect('index')


def orar_view(request, grupa):
    username = 'NoOne'
    logged = False
    if 'username' in request.session:
        username = request.session['username']
        logged = True
    else:
        return redirect('/login_page')
    is_prof = False
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    td = [['<td></td>' for c in range(0, 7)] for line in range(0, 12)]

    if grupa is None:
        return render(request, 'orar.html',
                      {'grupa': grupa, 'linia1': td[0], 'linia2': td[1], 'linia3': td[2], 'linia4': td[3],
                       'linia5': td[4], 'linia6': td[5], 'linia7': td[6], 'linia8': td[7], 'linia9': td[8],
                       'linia10': td[9], 'linia11': td[10], 'linia12': td[11], 'logged': logged, 'username': username, 'is_prof':is_prof})

    _an = int(grupa[0])
    _gr = grupa[1:]

    td = of.get_orar_table(_an, _gr)
    return render(request, 'orar.html',
                  {'grupa': grupa, 'linia1': td[0], 'linia2': td[1], 'linia3': td[2], 'linia4': td[3], 'linia5': td[4],
                   'linia6': td[5], 'linia7': td[6], 'linia8': td[7], 'linia9': td[8], 'linia10': td[9],
                   'linia11': td[10], 'linia12': td[11], 'logged': True, 'username': username,'is_prof':is_prof})


def barfe_view(request):
    if 'username' in request.session:
        logged = True
    else:
        logged = False

    if logged == False:
        return redirect('/login_page')
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    if 'nume_tag' in request.POST:
        post = bf.get_posts(request,request.POST['nume_tag'])
    else:
        post = bf.get_posts(request,None)
    print post

    return render(request, 'barfe.html', {'logged': logged, 'username': request.session['username'], 'posts':post,'is_prof':is_prof})


def note_view(request):
    logged = False
    if 'username' in request.session:
        first_name = request.session['prenume']
        logged = True;
    else:
        return redirect('/login_page')
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    note = nf.get_note(request)

    obiect_chart=nf.get_obiect_chart(request)
    # obiect_chart=[{'nume':'mate','data':[0,0,0,0,1,2,3,4,5,6,2]},{'nume':'mlogica','data':[2,2,2,2,3,4,5,6,7,8,4]}]
    nume=[]
    data =[]
    for ob in obiect_chart:
        nume.append(ob['nume'])
        data.append(ob['data'])

    print note
    return render(request, 'note.html', {'materii': note, 'logged': logged,'nume':json.dumps(nume),'data':data})



def profesor_view(request):
    logged = False
    if 'username' in request.session:
        logged = True

    if logged == False:
        return redirect('/login_page')
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    materii = pf.get_materii(request)

    return render(request, 'profesor.html',
                  {'message': False, 'test': None, 'logged': logged, 'username': request.session['username'], 'materii':materii,'is_prof':is_prof})


def upload_excel(request):
    logged = False
    if 'username' in request.session:
        logged = True

    if logged == False:
        return redirect('/login_page')

    result = pf.upload(request)
    if result == -1:
        print 'Eroare'
    else:
        print 'success'

    return redirect('/profesor/')


def view_activate(request, name):
    print name
    stud = Student.objects.filter(token=name)
    print stud[0].user
    username = stud[0].user.username
    user = User.objects.filter(username=username)[0]
    user.is_active = True
    user.save()
    print name
    print stud[0].user.username
    return redirect('index')


def form_view(request):
    if 'username' not in request.session:
        redirect('login_page')
    else:
        logged = True
    profesori = []
    if 'username' in request.session:
        print request.session['username']
    if 'profesori' in request.session:
        # print request.session['profesori']
        profesori = request.session['profesori']
    else:
        profesori = pf.get_profs(request)

    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    nume = ""
    print 'POST'
    # print request.POST
    if 'profesori' in request.session and 'nume' in request.POST:
        form_functions.getResults(request)
        profesori = request.session['profesori']
    else:
        print "nu s-au dat date"
    if 'nume' in request.POST:
        nume = request.POST['nume']
        print nume
        for prof in profesori:
            if prof['nume'] == nume:
                prof['votat'] = True
    request.session['profesori'] = profesori
    # print request.session['profesori']
    return render(request, 'form.html', {"profesori": profesori,'logged':True,'is_prof':is_prof})


def facebook_view(request):
    return render(request,'facebook.html',{})

def facebook_view2(request):
    print "am primit requestul dde dupa logarea cu fb"
    print request.POST['accesToken']
    print "catre salvarea tokenului"
    request = af.save_user_token(request)
    #return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':False, 'error_message':None, 'need_token':False,'FACEBOOK_APP_ID':FACEBOOK_APP_ID,'groups':None,'is_prof':None})

    return HttpResponse("<p>Congrats you have logged with facebook </p>", mimetype='text/html')

def cont_view(request):
    if 'username' not in request.session:
        return redirect('/login_page')
    else:
        logged = True
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    groups = []
    u = User.objects.filter(username=request.session['username'])

    st = Student.objects.filter(user=u[0])
    if len(st) == 0:
        return redirect('login_page')

    if st[0].facebook_token != '':
        request.session['facebook_token'] = st[0].facebook_token
        request.session['facebook_userID'] = st[0].facebook_userID
        need_token = False
    else:
        need_token = True

    

    print need_token

    
    if need_token == False:
        groups = af.get_groups(request)

    return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':False, 'error_message':None, 'need_token':need_token,'FACEBOOK_APP_ID':FACEBOOK_APP_ID,'groups':groups,'is_prof':is_prof,'logged':logged})

def update_cont(request):
    if 'username' not in request.session:
        return redirect('/login_page')
    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False
    request = af.update_cont(request)
    if request.session['result'] == 0:
        return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':False,'error_message':None,'is_prof':is_prof})
    elif request.session['result'] == -1:
        return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':True,'error_message':'Parolele nu coincid','is_prof':is_prof})
    else:
         return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':True,'error_message':'Completeaza si repeat password','is_prof':is_prof})

def set_group_view(request):
    if 'username' not in request.session:
        return redirect('login_page')
    else:
        logged = True

    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    l = request.POST.getlist('grupuri[]')
    for item in l:
        db_wrap = connections['default']
        g = db_wrap.get_collection('acre_Fbg')
        g.insert({'group_id':item,'access_token':request.session['facebook_token'],'expira':'2010-03-29T11:20:40+0000'})
    need_token = False
    groups = []
    return render(request,'cont.html',{'Nume':request.session['nume'],'Prenume':request.session['prenume'], 'error':False, 'error_message':None, 'need_token':need_token,'FACEBOOK_APP_ID':FACEBOOK_APP_ID,'groups':groups,'is_prof':is_prof})

def view_recomandare(request):

    if 'username' in request.session:
        logged = True
    else:
        logged = False

    if 'prof' in request.session:
        if request.session['prof'] == 1:
            is_prof = True
        else:
            is_prof = False
    else:
        is_prof = False

    #print 'aici1'
    note = nf.get_note(request)
    print 'note'

    note2 = []
    for item in note:
        note2.append( (item['disciplina'],item['nota']) )


    materii = of.get_materii(2)
    #print 'aici2'
    result = rp.recommend(request)



    
    
    result2 = rn.getScore(note2,materii)
    print 'aici3'
    print result2
    #print 'aici4'
    result3 = fb_recomand.objects.filter()
    r3_int = []
    for item in result3:
        r3_int.append((item.scor, item.materie))

    result4 = []
    for item in sorted(r3_int, reverse=True):
        if item[0] > 0:
            result4.append({'materie':item[1], 'scor':''.join('+' for i in range(int(item[0] / 0.1)))})
        else:
            result4.append({'materie':item[1], 'scor':''.join('-' for i in range(int(item[0] / 0.1)))})


    #print result2

    
    for prop in result['prop']:
        print prop
    for user in result['studs']:
        print user
    #return HttpResponse(result,mimetype='application/json')
    return render(request,'recomandari.html',{'logged':logged,'is_prof':is_prof,'prof_result':result['prop'],'materii_result':result2,'fb_recomand':result4})