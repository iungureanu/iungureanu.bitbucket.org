__author__ = 'istanescu'

from django.contrib.auth.models import User
from acre.models import Student, Note, Materie, Catalog, Evaluare_profs, Evaluare, Profesor
from operator import itemgetter
from django.db import connections

def check_profs(profs, mat):
    ob = {}
    try:
        evaluations = Evaluare_profs.objects.filter(nr_matricol=mat)
        ob['nr_matricol'] = mat
        ob['match'] = 0
        ob['evaluari'] = []
        for ev in evaluations:
            ob['evaluari'].append(ev)

    except Exception as e:
        print e
    # print ob
    return ob


def recommend(req):
    data = get_lists(req)
    profs=data['profs']
    studs = data['studs']
    evals = data['evals']
    proposals=[]
    new_studs=[]
    print profs
    print studs
    for stud in studs:
        count =0.000
        match=0.000
        m_notare=0.000
        m_predare=0.000
        m_interactiune=0.000
        # print profs
        for eval in stud['evaluari']:
            # print eval.nume_profesor
            if eval.nume_profesor in profs:
                # print eval.nume_profesor + 'count'
                count+=1.000
                # print count
                # print eval.nume_profesor
                # print "matricol:" + stud['nr_matricol']
                e=eval.rezultate.overall
                for us_ev in evals:
                    if us_ev.rezultate.overall== e and us_ev.nume_profesor == eval.nume_profesor:
                        match+=1.000
                        if us_ev.rezultate.predare== eval.rezultate.predare and us_ev.nume_profesor == eval.nume_profesor:
                            m_predare+=1.000
                        if us_ev.rezultate.evaluare== eval.rezultate.evaluare and us_ev.nume_profesor == eval.nume_profesor:
                            m_notare+=1.000
                        if us_ev.rezultate.atmosfera== eval.rezultate.atmosfera and us_ev.nume_profesor == eval.nume_profesor:
                            m_interactiune+=1.000
        stud['profi_comuni']=count
        if count !=0.0 and match !=0.0:
            stud['matched']=match/count
            stud['predare']=m_predare/match
            stud['notare']=m_notare/match
            stud['atmosfera']=m_interactiune/match
            mean=(match+m_predare+m_notare+m_interactiune)/(match*3 + count)
            stud['mean']=mean
            print mean

            if stud['matched']<0.75:
            # print stud.nr_matricol
                studs.remove(stud)
                if stud['mean']<0.75:
                    studs.remove(stud)
            else:
                print 'ELSE'
                for eval in stud['evaluari']:
                    if  eval.nume_profesor not in proposals and eval.rezultate.overall == 2.00:
                        proposals.append(eval.nume_profesor)
                        print 'AICIIIII'
                        print eval.nume_profesor
                        print proposals
            print studs
            new_studs = studs
            print proposals
    # print profs
    # for stud in studs:
        # print stud['mean']
        # print stud['matched']

    # print new_studs
    # print proposals
    db_wrap = connections['default']
    orar = db_wrap.get_collection('acre_orar')
    ob_proposals=[]
    print 'prop'
    print proposals
    for prof in proposals:
        materii=orar.find({'profesor':prof,'tip':{'$in':['Seminar','Laborator']}})
        ob={}
        ob['nume']=prof;
        ob['materii']=[]
        for materie in materii:
            mat=materie['disciplina']
            if mat not in ob['materii']:
                ob['materii'].append(mat)
        ob_proposals.append(ob)
    print 'propuneri'
    print ob_proposals
    return {'prop':ob_proposals,'studs':new_studs}
    
def get_lists(req):
    username = req.session['username']
    list_studs = []
    list_profs = []
    evals = []
    try:
        us = User.objects.filter(username=username)
        stud = Student.objects.filter(user=us[0])
        try:
            an_studiu = stud[0].an_studiu
            ev_profi = Evaluare_profs.objects.filter(nr_matricol=stud[0].nr_matricol)
            for ev in ev_profi:
                evals.append(ev)
                list_profs.append(ev.nume_profesor)
            students = Student.objects.raw_query({'an_studiu': {'$gte': an_studiu}})
            for stud in students:
                evaluations = check_profs(list_profs, stud.nr_matricol)
                if 'nr_matricol' in evaluations:
                    print 'evaluations'
                    print evaluations
                    list_studs.append(evaluations)
        except Exception as e:
            print e
            print 'the curent user has never given an opinion or there are no bigger users to have completed the form'
    except Exception as e:
        print e
    print len(list_studs)
    return {'profs': list_profs, 'studs': list_studs, 'evals': evals}