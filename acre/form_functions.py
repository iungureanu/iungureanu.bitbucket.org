__author__ = 'istanescu'
from django.contrib.auth.models import User
from acre.models import Student, Note, Materie, Catalog, Evaluare_profs, Evaluare, Profesor
import recomandare_prof as rp
from django.db import connections


def getResults(req):
    ob = req.POST
    session = req.session
    if 'atmosfera' in ob:
        atmosfera = int(ob['atmosfera'])
        interactiune = int(ob['interactiune'])
        explicatii = int(ob['explicatii'])
        materiale = int(ob['materiale'])
        subiecte = int(ob['subiecte'])
        evaluare = int(ob['evaluare'])
        overall = int(ob['overall'])

        predare = (explicatii + materiale) / 2.0
        if predare < 1.0:
            predare = 0.0
        elif predare < 2.5:
            predare = 1.0
        else:
            predare = 2.0

        notare = (subiecte + evaluare) / 2.0
        if notare < 1.0:
            notare = 0.0
        elif notare < 2.5:
            notare = 1.0
        else:
            notare = 2.0
        atmosfera = (subiecte + evaluare) / 2.0
        if atmosfera < 1.0:
            atmosfera = 0.0
        elif atmosfera < 2.5:
            atmosfera = 1.0
        else:
            atmosfera = 2.0
        if overall < 1.0:
            overall = 0.0
        elif overall < 2.5:
            overall = 1.0
        else:
            overall = 2.0
        try:

            us = User.objects.filter(username=session['username'])
            print us[0].first_name
            cat = Student.objects.filter(user=us[0])
            to_add = {}
            nr_mat = ""
            try:
                print cat
                user = cat[0]
                print user
                print "user"
                nr_mat = user.nr_matricol
                evv = {'atmosfera': atmosfera, 'predare': predare, 'notare': notare,
                       'overall': overall}
                db_wrap = connections['default']
                prof_eval = db_wrap.get_collection('acre_evaluare_profs')
                prof_eval.update({'nr_matricol': nr_mat, 'nume_profesor': ob['nume']},
                                 {'nr_matricol': nr_mat, 'nume_profesor': ob['nume'], 'rezultate': evv},
                                 True)
                print ob['nume']
            except Exception as e:
                evv = {'atmosfera': atmosfera, 'predare': predare, 'notare': notare,
                       'overall': overall}
                db_wrap = connections['default']
                prof_eval = db_wrap.get_collection('acre_evaluare_profs')
                prof_eval.update({'nr_matricol': nr_mat, 'nume_profesor': ob['nume']},
                                 {'nr_matricol': nr_mat, 'nume_profesor': ob['nume'], 'rezultate': evv},
                                 True)
        except Exception as e:
            print e

    else:
        print "nada"