import parser_note_excel as PNE
from acre.models import Orar
from acre.models import Note, Catalog, User, Orar, Student
import re


def upload(request):
    ze_dir = './upload/excel/' + request.session['username'] + '_' + str(request.FILES['note'])
    print ze_dir
    with open(ze_dir, 'wb+') as destination:
        for chunk in request.FILES['note'].chunks():
            destination.write(chunk)
    curs = ''
    if 'cursuri' in request.POST:
        curs = request.POST['cursuri']
    else:
        return -1

    notee = []

    pn = PNE.ParserNoteExcel()
    pn.adaugaNote(curs, ze_dir)
    for elev in pn.elevi:
        for materie in pn.elevi[elev]:
            notee.append(Note(nr_matricol=elev.nr_matricol, nota=materie.nota))
    c = Catalog(an_universitar=request.POST['an_universitar'], semestru=request.POST['semestru'], nume_materie=curs,
                an_studiu=request.POST['an_studiu'], note=notee)
    Catalog.save(c)
    return 0


def get_materii(request):
    nume_profesor = ''
    if 'prof' in request.session:
        nume_profesor = request.session['nume'] + ' ' + request.session['prenume']

    materii = Orar.objects.raw_query({'profesor': {'$regex': nume_profesor}})
    result = []
    dct = {}
    for m in materii:
        if m.disciplina not in dct:
            result.append(m.disciplina)
            dct[m.disciplina] = 1
    return result


def get_profs(request):
    # mai intai aflam anul studentului si apoi ii luam toti profii care predau la anul lui
    # acolo unde punem evaluarile sa fac cu update daca e acelasi prof
    profesori = []
    try:
        us = User.objects.filter(username=request.session['username'])
        print us[0].first_name
        cat = Student.objects.filter(user=us[0])
        an_studiu = 0
        try:
            an_studiu = cat[0].an_studiu
            ore = Orar.objects.filter(an=an_studiu)
            if (len(ore)) != 0:
                for ora in ore:
                    if ora.tip != 'Curs':
                        prof={}
                        prof={'nume':ora.profesor,'votat':False}
                        print 'bagam proful'
                        try:
                            profesori.index(prof)
                        except Exception as e:
                            print e
                            profesori.append(prof)
        except Exception as e:
            print e
            print 'no student'

    except Exception as e:
        print e
        print 'no user'
    return profesori