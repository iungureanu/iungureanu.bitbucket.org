from django.contrib.auth import authenticate
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from acre.models import Student, Profesor, Fbg
from django.core.mail import EmailMultiAlternatives

from validate_email import validate_email  # this one should be installed via pip also pyDNS
import urllib2
import json

def register(request, tip):

    if tip == 'elev':
        username = request.POST['email']
        password = request.POST['passwordbranza']
        email = request.POST['email']
        last_name = request.POST['nume']
        first_name = request.POST['prenume']
        group = request.POST['grupa']
        nr_matricol = request.POST['matricol']
        year = request.POST['an']
        result = "ihasd"
        user = None
        user1 = None

        if User.objects.filter(username=username):
            # result = 'User already exists'
            request.session['result'] = -1
        else:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.is_active = False
            user.save()
            token = ""
            try:
                # for us in User.objects.filter(username=username):
                token = nr_matricol
                # print us
                print token
                subject, from_email, to = 'acre Confirmation', 'acre.fii@gmail.com', username
                text_content = 'This is an important message.'
                message = "<p>Click the follwoing link for confirmation <p> <a href=\"http://localhost:8000/activate/" + token + "\">click me for Acre account activation/</a>"
                html_content = message
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            # send_mail('Acre', "mesaj cu token linkk wtvs", 'fii.project.acre@gmail.com', ['ionutungureanu93@gmail.com',username], fail_silently=False, auth_user=None, auth_password=None, connection=None,html_message= message)
            except Exception as e:
                print e
            print token
            student = Student(token=token, user=user, nr_matricol=nr_matricol, an_studiu=year, grupa=group)
            student.save()
            user1 = authenticate(username=username, password=password)
            if user1 is not None:
                # the password verified for the user
                if user1.is_active and user.is_authenticated:
                    # result = "nr matricol stundet ; " + student.nr_matricol
                    request.session['result'] = 0
                    request.session['username'] = username
                    request.session['first_name'] = user.first_name
                    request.session['last_name'] = user.last_name
                    request.session['prof'] = 0
                else:
                    # result = "inactive user"
                    request.session['result'] = -2
            else:
                # the authentication system was unable to verify the username and password
                # result = "The username and password were incorrect."
                request.session['result'] = -3
    else:
        username = request.POST['email']
        password = request.POST['passwordbranza']
        email = request.POST['email']
        last_name = request.POST['nume']
        first_name = request.POST['prenume']
            
        result = "ihasd"
        user = None
        user1 = None
        if User.objects.filter(username=username):
            # result = 'User already exists'
            request.session['result'] = -1
        else:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.is_active = False
            user.save()
            token = ""
            try:
                    # for us in User.objects.filter(username=username):
                token = email
                    # print us
                print token
                subject, from_email, to = 'acre Confirmation', 'acre.fii@gmail.com', username
                text_content = 'This is an important message.'
                message = "<p>Click the follwoing link for confirmation <p> <a href=\"http://localhost:8000/activate/" + token + "\">click me for Acre account activation/</a>"
                html_content = message
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                # send_mail('Acre', "mesaj cu token linkk wtvs", 'fii.project.acre@gmail.com', ['ionutungureanu93@gmail.com',username], fail_silently=False, auth_user=None, auth_password=None, connection=None,html_message= message)
            except Exception as e:
                print e
            
            materii_curs = []
            materii_laborator = []
            prof = Profesor(user=user)
            prof.save()
            user1 = authenticate(username=username, password=password)
            if user1 is not None:
                    # the password verified for the user
                if user1.is_active and user.is_authenticated:
                        # result = "nr matricol stundet ; " + student.nr_matricol
                    request.session['result'] = 0
                    request.session['username'] = username
                    request.session['first_name'] = user.first_name
                    request.session['prof'] = username
                else:
                        # result = "inactive user"
                    request.session['result'] = -2
            else:
                    # the authentication system was unable to verify the username and password
                    # result = "The username and password were incorrect."
                request.session['result'] = -3
    return request


def user_login2(request):
    username = request.POST['email']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    logged = False
    #request.session['result'] = -2
    if user is not None:
        # the password verified for the user
        first_name = user.first_name
        if user.is_active:
            login(request=request, user=user)
            logged = user.is_authenticated()
            if logged == True:
                request.session['username'] = username
                request.session['nume'] = user.last_name
                request.session['prenume'] = user.first_name
                request.session['prof'] = 0
                request.session['result'] = 0

                st = Student.objects.filter(user=user)
                if len(st) == 1:
                    var = str(st[0].an_studiu) + st[0].grupa
                    request.session['grupa'] = var

                else:
                    prof = Profesor.objects.filter(user=user)
                    if len(prof) == 1:
                        request.session['prof'] = 1
                        request.session['nume'] = user.last_name
                        request.session['prenume'] = user.first_name
                print 'ok'
        else:
            request.session['result'] = -1
            print '-1'
            # result = "The password is valid, but the account has been disabled!"
    else:
        # the authentication system was unable to verify the username and password
        # result = "The username and password were incorrect."
        request.session['result'] = -2
        print '-2'


    


def log_out(request):
    logout(request)
    if 'username' in request.session:
        del request.session['username']
    if 'logged' in request.session:
        del request.session['logged']
    if 'first_name' in request.session:
        del request.session['first_name']
    if 'prof' in request.session:
        del request.session['prof']
    if 'result' in request.session:
        del request.session['result']
    del request.session
    return request


def save_user_token(req):
    print "salvam tokenul vietii"
    try:
            us = User.objects.filter(username=req.session['username'])
            cat = Student.objects.filter(user=us[0])
            nr_mat=""
            try:
                nr_mat = cat[0].nr_matricol
                print req.POST['accesToken']
                print req.POST['userID']
                Student.objects.filter(nr_matricol=nr_mat).update(facebook_token=req.POST['accesToken'], facebook_userID=req.POST['userID'])
                req.session['access_token'] = req.POST['accesToken']
                req.session['facebook_userID'] = req.POST['userID']
            except Exception as e:
                print e
                print "exceptie la update"
    except Exception as e:
        print e
        print "Exceptie la identificarea studentului"
    return req


def update_cont(request):
    nume = request.POST['nume']
    prenume = request.POST['prenume']
    password = request.POST['password']
    password_repeat = request.POST['password_repeat']

    if nume != '':
        User.objects.filter(username=request.session['username']).update(last_name=nume)
        request.session['result'] = 0
        request.session['nume'] = nume
    if prenume != '':
        User.objects.filter(username=request.session['username']).update(first_name=prenume)
        request.session['prenume'] = prenume
    if password != '':
        if password_repeat != '':
            if password == password_repeat:
                User.objects.filter(username=request.session['username']).update(password=password)
                request.session['result'] = 0
            else:
                request.session['result'] = -1
        else:
            request.session['result'] = -2
    return request

def get_groups(request):
    userid = request.session['facebook_userID']
    acces_token = request.session['facebook_token']
    request = 'https://graph.facebook.com/' + userid + '/groups?access_token=' + acces_token
    groups = []
    try:
        response = urllib2.urlopen(request)
        data = json.loads(response.read())
        for entry in data['data']:
            groups.append({'id':entry['id'],'name':entry['name']})
    except Exception as exc:
        print 'Graph api request has failed! => ' + repr(exc)
    print groups
    return groups