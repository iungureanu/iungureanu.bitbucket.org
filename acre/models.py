__author__ = 'istanescu'
from django.contrib.auth.models import User
from django.db import models
from djangotoolbox.fields import EmbeddedModelField, ListField
from django.core.validators import MaxValueValidator, MinValueValidator
from django_mongodb_engine.contrib import MongoDBManager


class Fbg(models.Manager):
    group_id = models.TextField()
    access_token = models.TextField()
    expira = models.DateField()
    objects = MongoDBManager()


class Catalog(models.Model):
    an_universitar = models.TextField()
    semestru = models.IntegerField()
    nume_materie = models.TextField()
    an_studiu = models.IntegerField()
    note = ListField(EmbeddedModelField('Note'))
    objects = MongoDBManager()


class Materie(models.Model):
    an_studiu = models.IntegerField()
    semestru = models.IntegerField()
    nume = models.TextField()
    objects = MongoDBManager()


class Note(models.Model):
    nr_matricol = models.TextField()
    nota = models.IntegerField(default=0,
                               validators=[
                                   MaxValueValidator(10),
                                   MinValueValidator(0)
                               ])
    objects = MongoDBManager()

class Evaluare_profs(models.Model):
    nr_matricol=models.TextField()
    rezultate=EmbeddedModelField('Evaluare')
    nume_profesor=models.TextField()
    objects = MongoDBManager()

class Evaluare(models.Model):
    atmosfera=models.FloatField()
    evaluare=models.FloatField()
    predare=models.FloatField()
    overall=models.IntegerField()
    objects = MongoDBManager()


class Student(models.Model):
    user = models.OneToOneField(User)
    nr_matricol = models.CharField(db_index=True, max_length=16)
    an_studiu = models.IntegerField()
    semestru = models.IntegerField(default=0)
    grupa = models.CharField(max_length=2)
    objects = MongoDBManager()
    token = models.TextField()
    facebook_token=models.TextField()
    facebook_userID = models.TextField()
    objects = MongoDBManager()

class Profesor(models.Model):
    user = models.OneToOneField(User)
    activat = models.IntegerField(default=0)
    objects = MongoDBManager()


class NoteManager(models.Manager):
    def update_nota(self, nr_matricol, disciplina, nota):
        detalii_nota = self.filter(nr_matricol=nr_matricol, disciplina=disciplina)
        detalii_nota.nota = nota


class OrarManager(models.Manager):
    def durata(self):
        interval = self.ora.split(' ')
        ora_inceput = interval[0]
        ora_final = interval[1]
        durata = int(ora_final) - int(ora_inceput)
        return durata


class Orar(models.Model):
    ora = models.CharField(max_length=5)
    disciplina = models.CharField(max_length=100)
    profesor = models.CharField(max_length=100)
    tip = models.CharField(max_length=10)
    ''',choices=('laborator','seminar','curs')'''
    sala = models.CharField(max_length=4)
    zi = models.CharField(max_length=10)
    an = models.IntegerField()
    grupa = models.CharField(max_length=3)
    objects = OrarManager()
    culoare = models.CharField(max_length=10)
    objects = MongoDBManager()
    
    def __unicode__(self):
        return u'%s %d %s %s %s %s \n' % (self.zi, self.an, self.grupa, self.ora, self.disciplina, self.tip)


class fb_recomand(models.Model):
    materie = models.TextField()
    scor = models.FloatField()
    suma = models.FloatField()
    numar_total = models.IntegerField()

class single_post(models.Model):
    id_autor = models.TextField()
    autor = models.TextField()
    mesaj = models.TextField()

class posturi(models.Model):
    tag = models.TextField()
    mesaj = EmbeddedModelField('single_post')
    comentarii = ListField(EmbeddedModelField('single_post'))
    objects = MongoDBManager()