#-------------------------------------------------------------------------------
# Name:        Parser Orar Info - Uaic
#
# Author:      rpx
#
# Created:     11/04/2015
# Copyright:   (c) Razvan Popa 2015
#-------------------------------------------------------------------------------

import urllib2
from re import findall
from lxml.html import parse
from copy import deepcopy
import os
import sys
import time
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "acre.settings")
sys.path.append('C:\Users\iungureanu\Documents\iungureanu.bitbucket.org')

from acre.models import Orar
import pymongo
from django.db import connections

class ParserOrar:
    def __init__(self):
        self.grupe = {}
        for semian in ['1A', '1B', '2A', '2B', '3A', '3B']:
            cursuri = set()
            page = parse('http://thor.info.uaic.ro/~orar/participanti/orar_I' + semian + '.html')
            rows = page.xpath("body/table")[0].findall("tr")
            for row in rows[1:]:
                content = [c.text_content() for c in row.getchildren()]
                if len(content) < 9:
                    ziua = content[0].strip()
                    continue
                entry = Entry()
                entry.ziua = ziua
                entry.de_la     = content[0].strip()
                entry.pana_la   = content[1].strip()
                entry.grupa     = content[2].strip()
                entry.disciplina= content[3].strip()
                entry.tip       = content[4].strip()
                entry.profesor  = ', '.join([c.strip() for c in content[5].strip().split('\r\n') if c.strip() != ''])
                entry.sala      = ', '.join([c.strip() for c in content[6].strip().split('\r\n') if c.strip() != ''])
                entry.frecv     = content[7].strip()
                entry.pachet    = content[8].strip()
                for grupa in [c.strip() for c in entry.grupa.split('\r\n') if c.strip() != '']:
                    entry.grupa = grupa
                    if len(entry.grupa) > 3:
                        if self.grupe.get(entry.grupa):
                            self.grupe[entry.grupa].add(entry)
                        else:
                            self.grupe[entry.grupa] = set([entry])
                    elif grupa in 'I' + semian:
                        cursuri.add(entry)
            for entry in cursuri:
                for grupa in self.grupe:
                    if semian in grupa:
                        new_entry = deepcopy(entry)
                        new_entry.grupa = grupa
                        self.grupe[grupa].add(new_entry)

class Entry:
    def __init__(self):
        self.__zile = ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica']

    def __hash__(self):
        return  (self.__zile.index(self.ziua) + 1) * 1000 + int(self.de_la[:2]) * 10 + ord(self.disciplina[0])

    def __gt__(self, entry2):
        if self.ziua != entry2.ziua:
            return self.__zile.index(self.ziua) > self.__zile.index(entry2.ziua)
        elif self.de_la != entry2.de_la:
            return self.de_la > entry2.de_la
        return self.disciplina > entry2.disciplina

    def __str__(self):
        return 'Ziua:\t\t' + self.ziua + '\nDe la:\t\t' + self.de_la + '\nPana la:\t' + self.pana_la + '\nDisciplina:\t' + self.disciplina + '\nTip:\t\t' + self.tip + '\nProfesor:\t' + self.profesor + '\nSala:\t\t' + self.sala + '\n'

def main():
    po = ParserOrar()
    culori = {} 
    _c = ['gri','green','blue','blue-light','purple','yellow','orange','orange2','red','green2','purple2']
    idx = 0
    for grupa in sorted(po.grupe):
        print 'Grupa:\t' + grupa + '\n'
        _an = grupa[1:2]
        _grupa = grupa[2:]
        print 'an '+_an
        print 'grupa '+_grupa
        for entry in sorted(po.grupe[grupa]):
            print str(entry) + '\n'
            ora = entry.de_la + " " + entry.pana_la
            if entry.disciplina in culori:
                cul = culori[entry.disciplina]
            else:
                cul = _c[idx]
                idx = (idx + 1) % 11
                culori[entry.disciplina] = cul
            p1 = Orar(ora = ora, disciplina = entry.disciplina, profesor = entry.profesor, tip = entry.tip, sala = entry.sala, zi=entry.ziua, an = _an, grupa=_grupa, culoare = cul)
            p1.save()

if __name__ == '__main__':
    
    activate = False
    while True:
        t = time.strftime("%m")
        r = Orar.objects.filter();
        if t == '10' or t == '02' or len(r) == 0:
            print 'Activate parser. Main function run every 1h'
            activate = True
        else:
            print 'Dezactivate parser until sem 2 starts'
            activate = False
        if activate == True:
            print 'main'
            db_wrap = connections['default']
            catalog = db_wrap.get_collection('acre_orar')
            catalog.remove()
            main()
            print 'sleep'
            time.sleep(1*60*60)

