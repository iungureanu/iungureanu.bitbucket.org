from acre.models import Orar

def indice_td(ora_inceput):
    _ora = int(ora_inceput[:2], 10)
    if _ora == 8:
        return 0
    if _ora == 9:
        return 1
    if _ora == 10:
        return 2
    if _ora == 11:
        return 3
    if _ora == 12:
        return 4
    if _ora == 13:
        return 5
    if _ora == 14:
        return 6
    if _ora == 15:
        return 7
    if _ora == 16:
        return 8
    if _ora == 17:
        return 9
    if _ora == 18:
        return 10

    return 11

def get_orar_table(_an,_gr):
	td = [['<td></td>' for c in range(0, 7)] for line in range(0, 12)]
	for o in Orar.objects.filter(an=_an, grupa=_gr):
		print 'an: ' + str(o.an) + ' gr: ' + o.grupa + '\n';
		if o.zi == 'Luni':
			td[indice_td(o.ora.split(' ')[0])][0] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Marti':
			td[indice_td(o.ora.split(' ')[0])][1] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Miercuri':
			td[indice_td(o.ora.split(' ')[0])][2] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Joi':
			td[indice_td(o.ora.split(' ')[0])][3] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Vineri':
			td[indice_td(o.ora.split(' ')[0])][4] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Sambata':
			td[indice_td(o.ora.split(' ')[0])][5] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'
		if o.zi == 'Duminica':
			td[indice_td(o.ora.split(' ')[0])][6] = '<td rowspan="' + '2' + '" class="' + o.culoare + '"><h5>' + o.disciplina + '</h5><h6>' + o.tip + '</br>' + o.profesor + '</br>' + o.sala + '</h6></td>'

	return td

def get_materii(an_studiu):
    materii = Orar.objects.filter(an=an_studiu)
    result = []
    d = {}
    for m in materii:
        if m.disciplina in d:
            pass
        else:
            d[m.disciplina]=1
            result.append(m.disciplina)
    return result