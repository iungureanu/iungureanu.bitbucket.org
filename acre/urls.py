from django.conf.urls import include, url
from django.contrib import admin
from acre.views import index, register_user, user_login,user_logout,orar_view,barfe_view, login_page_view, note_view,form_view,view_activate,profesor_view,upload_excel,register_prof,cont_view
from acre.views import facebook_view,facebook_view2
from acre.views import update_cont, set_group_view, view_recomandare


urlpatterns = [
    # Examples:
    # url(r'^$', 'acre.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name="index"),
    url(r'^registered/$', register_user),
    url(r'^registered_prof/$', register_prof),
    url(r'^login/$', user_login),
    url(r'^logout/$', user_logout),
    url(r'^orar/([123][AB][1234567890])/$', orar_view),
    url(r'^orar/$', orar_view,{'grupa':None}),
    url(r'^barfe/$', barfe_view),
    url(r'^login_page/$', login_page_view),
    url(r'^note/$',note_view),
    url(r'^form/$',form_view),
    url(r'^activate/([\w]+)/$', view_activate),
    url(r'^profesor/$',profesor_view),
    url(r'^facebook/$',facebook_view2),
    url(r'^upload_excel/$',upload_excel),
    url(r'^fb/', facebook_view, name='fb_app'),
    url(r'^cont/$', cont_view),
    url(r'^update_cont', update_cont),
    url(r'^set_group',set_group_view),
    url(r'^recomandari/$',view_recomandare),
]
