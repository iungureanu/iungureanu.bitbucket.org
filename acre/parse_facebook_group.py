#-------------------------------------------------------------------------------
# Name:        Parse Facebook Posts From Group
#
# Author:      Razvan Popa
#
# Created:     30/05/2015
#-------------------------------------------------------------------------------

from time import strptime, strftime
import urllib2
import json
import os
import sys
import time

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "acre.settings")
sys.path.append('C:\Users\iungureanu\Documents\iungureanu.bitbucket.org')
from acre.models import Fbg
from acre.models import Student, fb_recomand, single_post, posturi
from django.db import connections
import sentimentAnalyser as SA

__access_token = 'CAACEdEose0cBAJvruX8jjSKxZCaeVD05A8ZBuc0QB9bLVaqlu3WP43anNKbdwkQmcCwCiGy9gi7XzSQZCY7Wr8J1mqZBJn2ZAZCRioTPG6eZAoYBsicMfZAZBMHNZA6DOtH7cOZCRZA9SVdw7g8eZCjnhHu9RUIwnEaQPzDwSQNL9Yn1ZApWQz7qjlYzGXoOzjWrvOxAok7vgQVmIZBcCb3VS7xDZBCf'

__user_id = '1628440134034928'

class FacebookComment:
    def __init__(self, user_id, autor, message):
        self.user_id = user_id
        self.message = message
        self.autor = autor

class FacebookPost:
    def __init__(self, post_id, user_id, autor, updated_time, message, tag, comments):
        self.post_id = post_id
        self.user_id = user_id
        self.updated_time = updated_time
        self.message = message
        self.tag = tag
        self.comments = comments
        self.autor = autor

    def __str__(self):
        conv = 'post_id: ' + self.post_id + '\nuser_id: ' + self.user_id + '\n'
        conv += 'updated_time: ' + self.updated_time + '\n'
        conv += 'tag: ' + self.tag + '\n'
        conv += 'message: ' + self.message + '\n'
        conv += 'Comments:\n'
        for comment in self.comments:
            conv += '\tuser_id: ' + comment.user_id + '\n'
            conv += '\tmessage: ' + comment.message + '\n\n'
        return conv

__tags_dict = {'JAVA' : 'PA', 'DIDACTICA' : 'PEDAGOGIE', 'PSIH-ED' : 'PEDAGOGIE', 'DIDACTICA INFORMATICII' : 'PEDAGOGIE', 'PSIHOLOGIE' : 'PEDAGOGIE', 'PROBABILITATI&STATISTICA' : 'PS', 'RETELE' : 'RC', 'WEB' : 'TW', 'ASM' : 'ACSO', 'MATE' : 'MATEMATICA', 'ALGORITMICA GRAFURILOR' : 'AG', 'CRIPTOGRAFIE' : 'CC', 'ALGORITMI GENETICI' : 'AG'}

__tags = set(['WEB', 'IP', 'PF', 'FAI', 'DIDACTICA', 'SD', 'MATE', 'JAVA', 'PSIH-ED', 'LFAC', 'ACSO', 'PA ', 'PSIHOLOGIE', 'ENG', 'PROBABILITATI&STATISTICA', 'BD', 'PLP', 'RETELE', 'LOGICA', 'GRAFURI', 'PL', 'WEB', 'ASM', ' POO ', 'SO', 'MATLAB', 'CC', 'RC', 'SGBD', 'PEDAGOGIE', 'MATE', 'POO', 'ALGORITMICA GRAFURILOR', 'PA', 'TW', 'PS', 'CRIPTOGRAFIE', 'ALGORITMI GENETICI', 'AG'])

def getTag(message):
    ib = message.find('[')
    ie = message.find(']')
    if ib == -1 or ie == -1:
        return ''
    ret_tag = message[ib : ie + 1]
    for tag in __tags:
        if tag in ret_tag:
            return  __tags_dict[tag] if __tags_dict.get(tag) else tag
    return ''

def getUpdates(last_update, group_id, acces_token):
    updates = []
    request = 'https://graph.facebook.com/' + group_id + '/feed?limit=100&access_token=' + acces_token
    while request != '':
        try:
            response = urllib2.urlopen(request)
            data = json.loads(response.read())
            print 'request executat'
            for entry in data['data']:
                if strptime(entry['updated_time'], '%Y-%m-%dT%H:%M:%S+0000') < strptime(last_update, '%Y-%m-%dT%H:%M:%S+0000'):
                    print 'break'
                    break
                comments = []
                qq = []
                if entry.get('comments'):
                    for comment in entry['comments']['data']:
                        comments.append(FacebookComment(comment['from']['id'], comment['from']['name'], comment['message']))
                        qq.append(single_post(id_autor=comment['from']['id'], autor=comment['from']['name'], mesaj = comment['message']))
                        print 'comentariu appendat'
                if not entry.get('message'):
                    continue
                message = entry['message'].strip()
                tag = '[' + getTag(message) + ']'
                if tag != '[]':
                    updates.append(FacebookPost(entry['id'], entry['from']['id'], entry['from']['name'], entry['updated_time'], message, tag, comments))
                    print 'tag obtinut'
                    q= single_post(id_autor = entry['from']['id'], autor=entry['from']['name'], mesaj = message)

                    p = posturi(tag = tag, mesaj=q, comentarii=qq)
                    p.save()
            request = data['paging']['next'] if (data.get('paging') and data['paging'].get('next')) else ''
            print 'next page '
            print request
        except Exception as exc:
            print 'Graph api request has failed! => ' + repr(exc)
    return updates

def findRelevantGroups(user_id, access_token):
    groups_id = []
    try:
        response = urllib2.urlopen('https://graph.facebook.com/' + user_id + '/groups?access_token=' + access_token)
        data = json.loads(response.read())
        for group in data['data']:
            groups_id.append((group['name'], group['id']))
    except Exception as exc:
        print 'Graph api request has failed! => ' + repr(exc)

    return groups_id

def main():

    while(True):
        db_wrap = connections['default']
        catalog = db_wrap.get_collection('acre_Fbg')
        groups = catalog.find({})
        for g in groups:
            updates = getUpdates('2015-03-29T11:20:40+0000',g['group_id'],g['access_token'])
            materii = {}
            print 'separe mesaje de comment'
            for fp in updates:
                if fp.tag != '':
                    if materii.get(fp.tag):
                        materii[fp.tag].append(fp.message)

                    else:
                        materii[fp.tag] = [fp.message]
                    for fc in fp.comments:
                        materii[fp.tag].append(fc.message)

            print materii.keys()
            for materie in materii:
                print 'get mean'
                x = SA.getMean(materii[materie])
                print 'upload database'

                db_wrap = connections['default']
                fb = db_wrap.get_collection('acre_fb_recomand')
                fb.update({'materie': materie},{'materie':materie,'scor':x[0],'suma':x[1],'numar_total':x[2]})
                print materie + ':\t' + str(SA.getMean(materii[materie]))
    
if __name__ == '__main__':
    main()
