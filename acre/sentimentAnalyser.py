#-------------------------------------------------------------------------------
# Name:        Sentiment Analyser
#
# Author:      Razvan Popa
#
# Created:     31/05/2015
#-------------------------------------------------------------------------------
from goslate import Goslate
from urllib import quote_plus
import urllib2
import json

def encodeText(text):
    ret = ''
    for c in text:
        if c.isalnum():
            ret += c
        elif c == ' ':
            ret += '+'
    return ret.encode('ascii','ignore')

def getScore(text):
    try:
        gs = Goslate()
        text = gs.translate(text, 'en', source_language = 'ro')
        response = urllib2.urlopen('https://api.idolondemand.com/1/api/sync/analyzesentiment/v1?text=' + encodeText(text) + '&apikey=a5beca00-6463-4d29-9f35-f8664c647b07')
        data = json.loads(response.read())
        return (data['aggregate']['sentiment'], data['aggregate']['score'])
    except Exception as exc:
        print 'Sentiment analyser api request has failed! => ' + repr(exc)
    return (0, 0)


def getMean(text_array):
    sum  = 0.0
    count = 0
    for text in text_array:
        count += 1
        sum += getScore(text)[1]
    return (sum / count, sum, count)

def main():
    print getMean(['ma simt bine si beau bere fara alcool', 'vreau sa merg la baie', 'mi-e atat de somn frate'])

if __name__ == '__main__':
    main()
