from django.shortcuts import render
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth.models import User


def index(request):
    return render(request, 'index.html')
    # the html with the register form